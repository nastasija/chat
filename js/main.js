(function ($) {
    $( document ).ready(function() {
        
        $('body').css('height', $(window).height());
                
        $( "#post" ).submit(function(e) {
            e.preventDefault();
            
            /*newComment(e.target.text.value);*/
            
            const commentFields = getCommentFieldsFromFromTarget(e.target);
    
            console.log(commentFields);
            
            addCommentToView(commentFields)
        });
        
    });
    
    
    
    
    function addCommentToView(commentFields) {
  	const $newComment = $('.new-comment'); // juqery element;
    const $generatedComentElement = $(`<div>${commentFields.author}: ${commentFields.text} ${commentFields.date}</div>` ); 
    
    $generatedComentElement.appendTo($newComment); //append comment to dom
}

    
    function getCommentFieldsFromFromTarget(eventTarget) {
        const fields = {
            author: eventTarget.author.value,
            text: eventTarget.text.value,
            date: new Date()
        };
        return fields;
        console.log(fields);
    }
    
    
    
    /*function newComment (text) {
        const sukaArray = getArrayFromText(text);
        console.log(sukaArray);
    }
    
    function getArrayFromText(str) {
        let newArray = str.split('\n');
        return newArray;
    } */
    
})(window.$);
    
 

